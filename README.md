# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm install`

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Testing

### `node_modules/.bin/cypress open`

This is an extremely simple test using cypress which tests if the app loads and if there is a response to the types API call
