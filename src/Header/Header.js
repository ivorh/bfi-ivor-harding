import React from "react";

import './Header.scss';

const Header = (props) => (
  <header className="header">
    <h1>Ivor Harding - BFI Task</h1>
  </header>
)
export default Header;
