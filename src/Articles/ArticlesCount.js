import React from "react";

const ArticlesCount = (props) => (
  <div className="articles-count">
    Total Results: {props.total}<br/>
  </div>
)
export default ArticlesCount;
