import React from "react";
import { useEffect, useState } from "react";
import axios from 'axios';

import './Articles.scss';

import Card from "../Card/Card";
import ArticlesCount from "../Articles/ArticlesCount";

export default function ArticleList(props) {
  const [articles, setArticles] = useState([]);
  const [meta, setMeta] = useState([]);
  const [selectedAuthor, setSelectedAuthor] = useState(null);
  const [loading, setLoading] = useState(true);
  let searchParamString = '';
  let searchParams = [];

  useEffect(() => {
    if(props.selectedType) {
      searchParams['type'] = props.selectedType
    }
    if(selectedAuthor) {
      searchParams['author'] = selectedAuthor
    }

    let first = true;
    for(var index in searchParams) {
      if (first === true) {
        searchParamString += '?';
        first = false;
      } else {
        searchParamString += '&';
      }
      searchParamString += index + "=" + searchParams[index];
    }

    axios({
     method: 'GET',
     url: `https://content-store.explore.bfi.digital/api/articles${searchParamString}`
    }).then(res => {
      const articles = res.data.data;
      setArticles(articles);
      const meta = res.data.meta;
      setMeta(meta);
      setLoading(false);
    })
  }, [props.selectedType, selectedAuthor]);

  let noPostsMessage = '';
  if (!articles.length && loading===false) {
    noPostsMessage = "No posts available for selected filters";
  }

  return (
    <div className="articles">
      {loading ? 'Loading...' : <ArticlesCount total={meta['total']}/>}
      {noPostsMessage}
      <div className="articles__wrap">
        <div className="articles__row">
          {articles.map((article) => (
            <Card
              title={article.title}
              summary={article.summary}
              content_type={article.content_type}
              created={article.created}
              image_url={article.primary_image.url}
              image_alt={article.primary_image.alt_text}
              authors={article.authors}
              path={article.path}
              key={article.uuid}
              setSelectedAuthor={setSelectedAuthor}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
