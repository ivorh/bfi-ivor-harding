import React from 'react';
import {useState} from 'react';

import './App.scss';

import Header from "./Header/Header";
import TypeList from "./Types/Types";
import ArticleList from "./Articles/Articles";

export default function App() {
  const [selectedType, setSelectedType] = useState(null);

  return (
    <div className="App">
      <Header />
      <TypeList setSelectedType={setSelectedType}/>
      <ArticleList selectedType={selectedType}/>
    </div>
  );
}
