import React from "react";

const Author = ({ id, name }) => (
  <div className="author">
    {name}
  </div>
);
export default Author;
