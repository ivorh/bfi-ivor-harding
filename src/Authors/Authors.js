import React from "react";
import axios from 'axios';

import Author from "./Author";

export default class TypeList extends React.Component {

  state = {
    authors: []
  }

  componentDidMount() {
    axios.get(`https://content-store.explore.bfi.digital/api/authors`)
      .then(res => {
        const authors = res.data.data;
        this.setState({ authors });
      })
  }

  render() {
    return (
      <div>
        <ul>
          {this.state.authors.map((author) => (
            <Author
              id={author.id}
            />
          ))}
        </ul>
      </div>
    );
  }
}
