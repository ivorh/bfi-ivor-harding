import React from "react";

import './Card.scss';

const Card = (props) => (
  <div className="card">
    <div className="card__wrapper" key={props.key}>
      <div className="card__image">
      <div className="card__image-wrapper">
        <img src={props.image_url} alt={props.image_alt} />
      </div>
      </div>
      <div className="card__content">
        <p className="card__content-type">{props.content_type}</p>
        <h3><a href={props.path}>{props.title}</a></h3>
        <ul className="authors">
          {
            props.authors &&
            props.authors.map((author) => {
              return (
                <li key={author.id} onClick={() => props.setSelectedAuthor(author.id)}>{ author.name }</li>
              )
            })
          }
        </ul>
        <p className="card__content-created">{props.created}</p>
        <p>{props.summary}</p>
      </div>
    </div>
  </div>
);
export default Card;
