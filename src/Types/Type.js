import React from "react";

const Type = props => {
  return (
    <li key="1234"><button onClick={() => props.setSelectedType(props.id)}>{props.name}</button></li>
  )
};
export default Type;
