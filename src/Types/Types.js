import React from "react";
import {useState, useEffect} from 'react';
import axios from 'axios';

import Type from "./Type";

import './Types.scss';

export default function TypeList(props) {
  const [availableTypes, setAvailableTypes] = useState([]);

  useEffect(() => {
    axios({
     method: 'GET',
     url: 'https://content-store.explore.bfi.digital/api/types'
    }).then(res => {
      const types = res.data.data;
      setAvailableTypes(types);
    })
  }, []);

  return (
    <div className="types-list">
      <ul>
        {availableTypes.map((type) => (
          <Type
            id={type.id}
            name={type.name}
            key={type.id}
            setSelectedType={props.setSelectedType}
          />
        ))}
      </ul>
    </div>
  );
}