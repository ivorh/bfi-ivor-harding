describe('Cypress', () => {
  it('is working', () => {
    expect(true).to.equal(true)
  });
  it('visits the app', () => {
    cy.visit('http://localhost:3000')
  })
})

describe('Request', () => {
  it('displays categories from API', () => {
    cy.request('https://content-store.explore.bfi.digital/api/types')
    .should((response) => {
      expect(response.status).to.eq(200)
      expect(response).to.have.property('headers')
      expect(response).to.have.property('duration')
    })
  })
})
